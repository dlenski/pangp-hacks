from time import strftime, time
from binascii import hexlify
from sys import stderr

def hexdump(bytes, width=32, text=True, offset=0):
    out = ''
    for pos in range(0, len(bytes), width):
        row = bytes[pos:pos+width]
        hexrow = ' '.join('%02x'%b for b in row)
        textrow = ' '+repr(row) if text else ''
        out += '%08x  %s%s\n' % (pos+offset, hexrow.ljust(3*width), textrow)
    return out

def tcp_start(flow):
    d = flow.dumpfile = open('/tmp/gpstflow-%s.pcap.txt' % strftime('%Y%m%d_%H%M%S'), 'w')
    print("Dumping to %s" % d.name, file=stderr)
    flow.dumpfile.write('#run this through text2pcap as: text2pcap -e 0x800 -t:%%s -Dn %s %s\n' % (d.name, d.name[:-4]))

def tcp_message(flow):
    d = flow.dumpfile
    m = flow.messages[-1]
    c = m.content
    cc = m.content[16:]

    d.write('%s:%d\n' % (('O' if m.from_client else 'I'), m.timestamp))

    magic = c[0:4]
    ethertype = c[4:6]
    clen = (c[6]<<8) + c[7]
    magic2 = c[8:16]

    pos = 0
    if magic==b"\x1a\x2b\x3c\x4d" and magic2==b"\x01\0\0\0\0\0\0\0":
        if clen != len(cc):
            print("GPST packet length mismatch (header %d, actual %d)" % (clen, len(cc)), file=stderr)
        # Add Ethernet header (0 for dest/source MAC) and EtherType captured from the packet
        # https://en.wikipedia.org/wiki/Ethernet_frame#Ethernet_packet_%E2%80%93_physical_layer
        # EtherType should be 0x0800 for IPv4 (known) or 0x86DD for IPv6 (conjectured)
        d.write('00000000 ' + ''.join(' %02x'%b for b in ((b'\0'*12) + ethertype)) + '\n')
        pos = 14
        d.write('%08x '%pos)
        d.write(''.join(' %02x'%b for b in cc) + '\n')
    else:
        cc = c
        d.write("# Non-GPST packet\n")
        d.write('00000000 ' + ''.join(' %02x'%b for b in ((b'\0'*12) + b'\x88\xb5')) + '\n')
        d.write(hexdump(c, offset=5))

	# Some versions of the official GP client have a bug where they can't access the GP gateway
        # via a proxy (e.g. MITMproxy) unless its hostname has been translated to an IP
        # address. <shrug>
        gateway_servers = {
           'gateway1.company.com': '127.0.0.1',
           'gateway2.company.com': '127.0.0.2',
        }
        nc = m.content
	for h, ip in gateway_servers.items():
            nc = nc.replace(b'<entry name="%s">'%h.encode(),b'<entry name="%s">'%ip.encode())
        if nc!=m.content:
            print("Replaced gateway entries due to proxy DNS bug", file=stderr)
            m.content = nc

        # It may be necessary to disable the portal's CA so that GP clients don't try to
        # verify gateway certificates against it... and then object because of the MITM.
        nc2 = nc.replace(b'<root-ca',b'<not-root-ca').replace(b'</root-ca',b'</not-root-ca')
        if nc2!=nc:
            print("Broke <root-ca>", file=stderr)
            m.content = nc2

def tcp_error(flow):
    d = flow.dumpfile
    error = flow.error
    d.write('# %s\n' % repr(error))

def tcp_end(flow):
    d = flow.dumpfile
    d.close()
