#!/usr/bin/env python3
import ssl
import hashlib
import random
from flask import Flask, request, abort, redirect, make_response

context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.load_cert_chain('server.pem')

app = Flask(__name__)
app.config.update(
    HOST = 'localhost',
    PORT = 10443,
    AUTHCOOKIE = hashlib.md5(bytes(random.randint(0,10))).hexdigest(),
)

@app.route('/')
def root():
    return redirect('/remote/login')

@app.route('/remote/login')
def login():
    return 'login html'

@app.route('/remote/logincheck', methods=('GET','POST',))
def logincheck():
    if 'code' not in request.form: # and random.randint(1,10)>5:
        return ('ret=2,reqid=198233047,polid=1-1-17689520,grp=RR'
                ',portal=RR,magic=1-17689520,tokeninfo=,'
                'chal_msg=Please enter your token code')
    else:
        resp = make_response('successful login html')
        resp.set_cookie('SVPNCOOKIE', AUTHCOOKIE)
        return resp

app.run(host=app.config['HOST'], port=app.config['PORT'],
        debug=True, ssl_context=context)
