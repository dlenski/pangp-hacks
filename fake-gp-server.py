#!/usr/bin/env python3
import ssl
import hashlib
import random
from flask import Flask, request, abort

context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.load_cert_chain('server.pem')

app = Flask(__name__)
app.config.update(
    PORT = 4443, PORT2 = 4443,
    HOST = 'localhost', HOST2 = '0::1',
    USER = 'nobody',
    PASS_PORTAL = 'nothing', PASS_GATEWAY = 'nothing2',
    CHALLENGE = ''.join(random.choice('abcdef0123456789') for x in range(4)),
    TOKEN = ''.join(random.choice('0123456789') for x in range(6)),
    AUTHCOOKIE = hashlib.md5(bytes(random.randint(0,10))).hexdigest(),
)

@app.route('/global-protect/prelogin.esp', methods=('GET','POST',))
@app.route('/ssl-vpn/prelogin.esp', methods=('GET','POST',))
def prelogin():
    return '''
<prelogin-response>
<status>Success</status>
<ccusername/>
<autosubmit>false</autosubmit>
<msg/>
<newmsg/>
<authentication-message>OH HAI I CAN HAZ CRUDENSHULS FOR {}?</authentication-message>
<username-label>Usernomnomnom</username-label>
<password-label>Paßwört</password-label>
<panos-version>1</panos-version>
<!-- <saml-auth-method>REDIRECT</saml-auth-method>
<saml-request>aHR0cDovL2xvbGNhdHMuY29tL2xvZ2luL3Zwbg==</saml-request> -->
<region>US</region>
</prelogin-response>'''.format(request.path)

@app.route('/global-protect/getconfig.esp', methods=('POST',))
def portal_config():
    user = request.form['user']
    passwd = request.form['passwd'] 
    inputStr = request.form.get('inputStr', '')
    if user == app.config['USER'] and passwd == app.config['PASS_PORTAL'] and inputStr == '':
        chance = random.randint(1,5)
        if chance <= 3:
            return '''<?xml version="1.0" encoding="UTF-8" ?>
            <policy><gateways><external><list><entry name="{}:{}"><description>1TestGateway</description></entry>
            <entry name="{}:{}"><priority>1</priority><manual>yes</manual><description>2TestGateway</description></entry>
            </list></external></gateways></policy>'''.format(app.config['HOST'], app.config['PORT'], app.config['HOST2'], app.config['PORT2'])
        elif chance==4:
             return '''
             var respStatus = "Challenge";
             var respMsg = "JavaScript sez: Enter your token code (hint: it's {})";
             thisForm.inputStr.value = "{}";
             '''.format(app.config['TOKEN'], app.config['CHALLENGE'])
        elif chance==5:
             return '''<challenge>
             <user>{}</user>
             <inputstr>{}</inputstr>
             <respmsg>XML sez: Enter your token code (hint: it's {})</respmsg>
             </challenge>'''.format(app.config['USER'], app.config['CHALLENGE'], app.config['TOKEN'])

    elif user == app.config['USER'] and passwd == app.config['TOKEN'] and inputStr == app.config['CHALLENGE']:
         # condensed portal XML
         return '''<?xml version="1.0" encoding="UTF-8" ?>
             <policy><gateways><external><list><entry name="{}:{}"><description>1TestGateway</description></entry>
             <entry name="{}:{}"><priority>1</priority><manual>yes</manual><description>2TestGateway</description></entry>
             </list></external></gateways></policy>'''.format(app.config['HOST'], app.config['PORT'], app.config['HOST2'], app.config['PORT2'])
    else:
        if random.randint(0,5):
            return 'Invalid username or password', 512
        else:
            return 'Invalid client certificate', 513

@app.route('/ssl-vpn/login.esp', methods=('POST',))
def gateway_login():
    user = request.form['user']
    passwd = request.form['passwd']
    inputStr = request.form.get('inputStr', '')
    if user == app.config['USER'] and (passwd, inputStr)==(app.config['PASS_GATEWAY'], ''): #, (app.config['TOKEN'], app.config['CHALLENGE'])):
        return '''<?xml version="1.0" encoding="utf-8"?> <jnlp> <application-desc>
            <argument>(null)</argument>
            <argument>{}</argument>
            <argument>deadbeefdeadbeefdeadbeefdeadbeefdeadbeef</argument>
            <argument>TestGateway</argument>
            <argument>{}</argument>
            <argument>TestAuth</argument>
            <argument>vsys1</argument>
            <argument>TestDomain</argument>
            <argument>(null)</argument>
            <argument/>
            <argument></argument>
            <argument></argument>
            <argument>tunnel</argument>
            <argument>-1</argument>
            <argument>4100</argument>
            <argument>1.2.3.4</argument>
            <argument/>
            <argument/>
            <argument/>
            <argument/>
            <argument></argument>
            </application-desc></jnlp>'''.format(app.config['AUTHCOOKIE'], app.config['USER'])
    else:
        if random.randint(0,5):
            return 'Invalid username or password', 512
        else:
            return 'Invalid client certificate', 513

@app.route('/ssl-vpn/getconfig.esp', methods=('POST',))
def gateway_config():
    if random.randint(0,1):
        return '<?xml version="1.0" encoding="utf-8"?><response status="success"/>'
    else:
        return '<?xml version="1.0" encoding="utf-8"?><response status="error"><error>Akthwap</error></response>'

#@app.errorhandler(512)
#def baduserpass(error):
#    return 'Invalid username or password'
#
#@app.errorhandler(513)
#def baduserpass(error):
#    return 'Invalid client certificate'
        
app.run(host=app.config['HOST'], port=app.config['PORT'], debug=True, ssl_context=context)
